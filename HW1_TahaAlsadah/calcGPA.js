function Calculate() {
var c1 = parseInt(document.getElementById('cr1').value);
var c2 = parseInt(document.getElementById('cr2').value);
var total = c1+c2;
 document.getElementById("totalCredits").innerHTML = total;
 // Compute GPA here
 var temp = 0; // Stores current weight of all courses

 var w1 = document.getElementById('g1').value;
 temp = temp + c1 * getWeight(w1);


 var w2 = document.getElementById('g2').value;
  temp = temp + c2 * getWeight(w2);

//var gpa = temp/total;

 // print GPA
  document.getElementById("gpa").innerHTML =  temp/total;
};
function getWeight(letterGrade){
  var weight = 0;
  switch (letterGrade){
    case "A+":
      weight = 4;
      break;
    case "A":
      weight = 3.75;
      break;
    case "B+":
      weight = 3.5;
      break;
    case "B":
      weight = 3.0;
      break;
    case "C+":
      weight = 2.5;
      break;
    case "C":
      weight = 2.0;
      break;
    case "D+":
      weight = 1.5;
      break;
    case "D":
      weight = 1.0;
      break;
    case "F":
      weight = 0.0;
      break;
      case "a+":
        weight = 4;
        break;
      case "a":
        weight = 3.75;
        break;
      case "b+":
        weight = 3.5;
        break;
      case "b":
        weight = 3.0;
        break;
      case "c+":
        weight = 2.5;
        break;
      case "c":
        weight = 2.0;
        break;
      case "d+":
        weight = 1.5;
        break;
      case "d":
        weight = 1.0;
        break;
      case "f":
        weight = 0.0;
        break;
    default:
    alert("Invalid grade, will be given 0 weight.")
    weight = 0.0;
    break;
  }
  return weight;
}
